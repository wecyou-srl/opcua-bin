# OpcUa

## OPCUA - Raspian buster on Raspberry PI4

#### Setup
```
sudo apt update
sudo apt install clang libcurl4 libpython2.7 libpython2.7-dev libcurl4-openssl-dev iptables hostapd dnsmasq

sudo systemctl disable dhcpcd.service
sudo rfkill unblock all

```

#### /etc/rc.local
```
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sysctl -w net.ipv4.ip_forward=1
exit 0
```

```
/etc/systemd/system/opcua.service
```

```
[Unit]
Description=OPCUA running on Raspberry PI4

[Service]
WorkingDirectory=/opt/opcua
ExecStart=/opt/opcua/run.sh pro sophia
Restart=always
RestartSec=10
KillSignal=SIGINT
SyslogIdentifier=OPCUA
User=root

[Install]
WantedBy=multi-user.target
```

# command

```
sudo systemctl enable opcua.service
```

