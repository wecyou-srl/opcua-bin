#!/bin/bash

ROOT_UID=0
if [ $UID != $ROOT_UID ]; then
    echo "You don't have sufficient privileges to run this script."
   exit 1
fi

if swift -version | grep -q '5.1.5'; then
   echo "swift-5.1.5-armv6-RPi01234-RaspbianBuster.tgz"
else
   echo "download and install swift-5.1.5-armv6-RPi01234-RaspbianBuster.tgz"
   wget https://github.com/uraimo/buildSwiftOnARM/releases/download/5.1.5/swift-5.1.5-armv6-RPi0123-RaspbianStretch.tgz
   tar xvzf swift-5.1.5-armv6-RPi0123-RaspbianStretch.tgz -C /
   rm -f swift-5.1.5-armv6-RPi0123-RaspbianStretch.tgz
fi

#ip link set wlan0 up
#ip addr flush dev wlan0
#ip addr add 10.3.141.1/24 dev wlan0

#iptables -F
#iptables -X
#iptables -t nat -F
#iptables -t nat -X
#iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
#iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
#iptables -t nat -A PREROUTING -p tcp --dport 22 -j ACCEPT
#iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

#sysctl -w net.ipv4.ip_forward=1

if [ "$1" == "sophia" ]
then
    apt install python3-pip
    pip3 install azure-iot-device
    ./OpcUa_Sophia
else
    ./OpcUa
fi
