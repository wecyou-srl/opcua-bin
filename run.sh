#!/bin/bash

ROOT_UID=0
if [ $UID != $ROOT_UID ]; then
    echo "You don't have sufficient privileges to run this script."
   exit 1
fi

git pull

./start.sh $1
