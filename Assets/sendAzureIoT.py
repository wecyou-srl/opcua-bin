#!/usr/bin/python3

# Using the Python Device SDK for IoT Hub:
#   sudo apt install python3-pip
#   sudo pip3 install azure-iot-device

import sys
from azure.iot.device import IoTHubDeviceClient, Message

CONNECTION_STRING = sys.argv[1]
JSON_BODY = sys.argv[2]

def iothub_client_init():
    # Create an IoT Hub client
    client = IoTHubDeviceClient.create_from_connection_string(CONNECTION_STRING)
    return client

def iothub_client_scada_run():
    try:
        client = iothub_client_init()
        message = Message(JSON_BODY)

        # Send the message.
        client.connect()
        #print( "Sending message: {}".format(message) )
        client.send_message(message)
        print( "Message successfully sent" )
        client.disconnect()
        
    except:
        print( "ERROR: Could not connect to Azure IoT Hub" )
        
if __name__ == '__main__':
    iothub_client_scada_run()
